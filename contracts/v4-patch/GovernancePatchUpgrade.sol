// SPDX-License-Identifier: MIT

pragma solidity ^0.6.0;
pragma experimental ABIEncoderV2;

import "../v1/Governance.sol";
import "../v3-relayer-registry/GovernanceStakingUpgrade.sol";

contract GovernancePatchProposal is GovernanceStakingUpgrade {
  mapping(uint256 => bytes32) public _proposalCodeHashes;

  event CodeHashDifferent(address target, bytes32 oldHash, bytes32 newHash);

  function execute(uint256 proposalId) external payable virtual override(Governance) {
    Proposal storage proposal = proposals[proposalId];

    if (proposal.target.codehash == _proposalCodeHashes[proposalId]) {
      super.execute(proposalId);
    } else {
      proposal.executed = true;
      emit CodeHashDifferent(proposal.target, _proposalCodeHashes[proposalId], proposal.target.codehash);
    }
  }

  function _propose(
    address proposer,
    address target,
    string memory description
  ) internal virtual override(Governance) {
    uint256 proposalId = super._propose(proposer, target, description);
    _proposalCodeHashes[proposalId] = target.codeHash;
  }
}
